%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
\documentclass[10pt]{article}


\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage{graph35}

\usepackage[a4paper, margin=1cm]{geometry}

\newcommand{\reponse}{\emph{Réponse : .......................}}

\setlength{\parindent}{0pt}

\begin{document}

\setcounter{section}{3}
\section{Échantillonnage}

\begin{activite}
  Un «~sourcier~» accepte de se préter à une expérience pour prouver ses capacités, et essaye de déterminer si un seau recouvert dont on ne voit pas l'intérieur contient ou non de l'eau.

  Sur 50 essais, il a trouvé la réponse correcte 30 fois.
    Le sourcier a-t-il un don ?

  Pour répondre à cette question, nous allons voir si une personne soumise au même test que le sourcier, mais répondant au hasard, pourrait obtenir le même taux de réussite que le prétendu sourcier.

  \begin{enumerate}
    \item En annonçant au hasard si le seau contient ou non de l'eau, quelle est la probabilité d'obtenir la bonne réponse ? \reponse
    \item Simuler une expérience : sur la calculatrice, tirer un nombre entre 0 et 1 au hasard :
      \key{OPTN}
      \function{PROB-b}
      \function{RAND-b}
      \function{Ran-b}.
      Si ce nombre est supérieur à 0,5, compter une bonne réponse (succès) ; sinon, compter une mauvaise réponse (échec). \reponse
    \item Le sourcier a eu droit à 50 essais. Simuler 50 expériences (en répétant les mêmes instructions qu'à la question précédente), et noter les résultats ci-dessous. Dans chaque case, mettre un cercle $\circ$ en cas de succès, et une croix $\times$ en cas d'échec.
      \begin{center}
        \begin{tabular}{*{25}{|c}|}
          \hline
          &&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
          &&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
        \end{tabular}
      \end{center}
      \emph{Nombre total de succès obtenus : \ldots}
    \item Partager les résultats avec l'ensemble de la classe (pour plus de lisibilité, laisser vide les cases contenant 0).
      \begin{center}
        \setlength\tabcolsep{1.5pt} % default value: 6pt
        \begin{tabular}{|l|*{26}{|c}|}
          \hline
          Succès & 0&1&2&3&4&5&6&7&8&9&10&11&12&13&14&15&16&17&18&19&20&21&22&23&24&25\\
          \hline
          Effectif & &&&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
          \hline
          Succès & 26&27&28&29&30&31&32&33&34&35&36&37&38&39&40&41&42&43&44&45&46&47&48&49&50&\\
          \hline
          Effectif & &&&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
        \end{tabular}
      \end{center}
    \item Combien de simulations ont produit autant ou plus de succès que le sourcier ? \reponse
    \item Le sourcier a-t-il fait preuve de son don ?
  \end{enumerate}
\end{activite}

\subsection*{Intervalle de fluctuation}

\begin{defprop}
  Pour un échantillon de taille $n\geq25$, et une proportion $p$ du caractère appartenant à $[0,2;0,8]$, la fréquence observée d'apparition d'un caractère dans l'échantillon appartient à l'\blanc{intervalle de fluctuation} \blanc{$\left[p-\frac{1}{\sqrt{n}};p+\frac{1}{\sqrt{n}}\right]$} avec une probabilité d'au moins \blanc{$0,95$}.
\end{defprop}

\begin{methode}[Prise de décision]
  Étant donné un échantillon de taille $n\geq25$, on se demande s'il est compatible avec un modèle donné, où le caractère apparait avec une probabilité $p\in\left[ 0,2;0,8 \right]$. On note $f$ la fréquence d'apparition du caractère dans cet échantillon.
  \begin{itemize}
    \item Si $f\notin\left[ p-\frac{1}{\sqrt{n}};p+\frac{1}{\sqrt{n}} \right]$, \phantom{on peut rejeter l'hypothèse que l'échantillon soit compatible avec le modèle.}
      \vspace{\stretch{1}}
    \item Si $f\in\left[ p-\frac{1}{\sqrt{n}};p+\frac{1}{\sqrt{n}} \right]$, \phantom{on ne peut pas rejeter l'hypothèse que l'échantillon soit compatible avec le modèle.}
        \vspace{\stretch{1}}
  \end{itemize}
\end{methode}

\begin{exemple}
  L'Assemblée nationale est actuellement composée 577 députés, dont 26 employés et ouvriers\footnote{Source : \emph{Liste des députés par catégorie socioprofessionnelle}, données publiées par l'Assemblée Nationale. \url{http://www2.assemblee-nationale.fr/deputes/liste/cat-sociopro}}, alors qu'ils représentent 47,7~\% des actifs occupés en 2016\footnote{Source : \emph{France, portrait social, édition 2017}. \url{https://www.insee.fr/fr/statistiques/3197279?sommaire=3197289}} (les autres étant des artisans, commerçants, chefs d'entreprise, cadres, professions intermédiaires, agriculteurs, etc.). La question que l'on se pose est : La proportion d'employés et ouvriers à l'Assemblée est-elle normale, ou ces catégories sont-elles sous représentées ?

  Pour simplifier le problème, on suppose que la proportion des employés et ouvriers (en activité ou non) parmi les personnes majeures en France est la même que parmi les actifs (c'est-à-dire 47,7~\%).

  \begin{enumerate}
    \item Supposons que l'on choisisse au hasard un échantillon de 577 personnes majeures dans la population française.
      \begin{enumerate}
        \item Quelle est la taille de l'échantillon ? Quelle est la probabilité qu'un individu choisi au hasard parmi les français majeurs actifs la population française soit un employé ou un ouvrier ?
        \item L'échantillon respecte-t-il les conditions de l'intervalle de fluctuation ?
        \item Déterminer un intervalle de fluctuation au seil de \SI{95}{\%} de cet intervalle.
      \end{enumerate}
    \item Quelle est la proportion d'employés et ouvriers à l'Assemblée nationale ?
    \item Conclure : Les employés et ouvriers sont-ils sous-représentés à l'Assemblée nationale ?
  \end{enumerate}
\end{exemple}

\end{document}
