---
title: Débat mouvant en SNT
---
description: Une séance de SNT, sous la forme d'un débat mouvant, pour discuter de l'affirmation « Si je n'ai rien à me reprocher, je n'ai rien à cacher ».
---
date: 2024-07-01
---
image: 20240520-090212.JPG
---
mastodon_id: 112712779257418399
---
tags:

débat
oral
SNT
---
body:

Dans mes cours de [SNT](/tag/snt), j'ai animé pendant quelques années des débats sur l'affirmation « Si je n'ai rien à me reprocher, je n'ai rien à cacher ». J'utilisais pour cela [une méthode](../20210403-organisation-de-debats-en-snt), conseillée par une collègue de SES, et expliquée dans un manuel, dans laquelle deux tables de quelques élèves doivent défendre un point de vue imposé, le tout animé par deux autres élèves, pendant que le reste de la classe observe.

J'avais publié ce travail sur une [liste de diffusion de professeur·e·s de SNT](https://groupes.renater.fr/sympa/arc/sciences-numeriques-technologie/2021-04/msg00000.html), et une réponse détaillée a été [apportée par Julien Peccoud](https://groupes.renater.fr/sympa/arc/sciences-numeriques-technologie/2021-04/msg00010.html) qui critiquait de manière pertinente mon travail, et proposait sa version (qui réutilisait d'ailleurs mon corpus de texte)[^partage].

[^partage]: Cher·e·s collègues, partagez votre travail ! Nous avons ici un exemple d'un travail que j'ai partagé, qui a été utilisé et amélioré par un collègue, que j'ai à nouveau utilisé et amélioré (ou adapté à ma guise). Rien de cela n'aurait été possible si nous n'avions pas mis notre travail à disposition du reste du monde. Si vous ne savez pas où ou comment partager vos ressources, jetez un œil à la [forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/).

Les critiques apportées à mon travail était :

- tous les élèves ne participent pas : certains sont spectateurs ;
- aucun travail n'est fait sur les *arguments*, et en particulier sur la différence entre *arguments* et *opinions* ;
- le débat travaillé est un « débat-spectacle » (comme les débats politiques vu à la télévision), où le but n'est pas de discuter avec l'adversaire, mais de rallier le public à sa cause (ce qui pousse indirectement à ne pas utiliser de bons arguments, mais de bonnes méthodes argumentatives, qu'elles soient [de bonne fois ou non](https://cortecs.org/la-zetetique/sophismes-une-petite-collection/)).

J'ai enfin trouvé le temps de retravailler cette séance (en partant du travail de mon collègue), et je la propose désormais sous la forme d'un [débat mouvant](https://www.reseau-canope.fr/fileadmin/user_upload/Projets/Valeurs_de_la_republique/EC_Le_debat_mouvant.pdf), qui permet de corriger les défaut cités plus haut :

- tous les élèves participent (même si ce n'est qu'en se positionnant sur la ligne) ;
- un travail est fait sur les arguments, et sur la différence entre argument et opinion ;
- la forme se rapproche d'avantage d'un débat où le but est d'arriver à un consensus, plutôt que de « gagner le débat ».

Tout ceci est décrit sur [mon site de SNT](https://snt.ababsurdo.fr/prof/les-donnees-structurees-et-leur-traitement/debat-mouvant/).
