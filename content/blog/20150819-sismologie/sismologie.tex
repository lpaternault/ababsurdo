% Pour compiler :
%$ lualatex $basename
% Beaucoup inspiré du travail de David Robert

% http://perpendiculaires.free.fr/wp-content/1S2014Chap15LoiBinomiale.pdf

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage[a5paper,margin=1.0cm]{geometry}

\pagestyle{empty}
\begin{document}

\begin{center}
  \large\textsc{Loi binomiale, Voyance et Sismologie}
  \hrule
\end{center}

\noindent\emph{Les données de cette activité sont issues de \emph{Devenez sorciers, devenez savants} (pages 105 à 107), d'Henri Broch et Georges Charpak, éditions Odile Jacob, 2002.}

Imaginons un individu se revendiquant astrologue, et faisant des prévisions pour les années à venir. En particulier, pour les années 1994, 1995 et 1996 (cette dernière année étant bissextile), il avait annoncé 169 jours sismiques dans le monde. Selon le \emph{National Earthquake Information Service} (États-Unis), au cours de cette période, sur le même territoire, il y a eu 196 jours de séisme (en ne considérant que ceux dont la magnitude est supérieure ou égale à 6,5), dont 33 coïncident avec les prédictions de notre voyant.
Nous allons étudier cette prédiction sous l'angle des probabilités.

L'hypothèse que nous allons étudier au cours de cette activité est : «~Les prédictions de l'astrologue sont-elles le fruit du hasard ?»

\section{Expérience élémentaire}

  On choisit une date au hasard parmi les trois années considérées (les dates étant équiprobables). Quelle est la probabilité d'obtenir une date de séisme ?

\section{Répétition d'expériences}

Dans toute la suite, on appelle «~succès~» (noté \textbf{S}) le fait d'obtenir une date de séisme, et «~échec~» (noté \textbf{E}) le faut d'obtenir une date sans séisme. On note, par exemple \textbf{SSE} l'évènement «~obtenir deux succès puis un échec~».

\subsection{Trois répétitions}

On répète l'expérience précédente trois fois de suite (c'est-à-dire : on choisit au hasard, trois dates).
\begin{enumerate}
  \item \emph{Évènements élémentaires}
\begin{enumerate}
  \item Dessiner l'arbre représentant cette nouvelle expérience.
  \item Calculer $P(SSE)$, $P(SES)$, $P(ESS)$. Quelle est la probabilité d'obtenir deux succès ?
  \item Calculer $P(SSS)$. Quelle est la probabilité d'obtenir trois succès ?
\end{enumerate}
\item On appelle $X$ la variable aléatoire correspondant au nombre de succès sur les trois répétitions.
  \begin{enumerate}
    \item Dresser la loi de probabilités de $X$.
    \item Calculer l'espérance de $X$, et interpréter ce résultat.
  \end{enumerate}
\end{enumerate}

\subsection{Quatre répétitions}

On répète maintenant l'expérience initiale quatre fois, et on note $Y$ le nombre de succès.

\begin{enumerate}
  \item Calculer $P(Y=0)$ et $P(Y=4)$.
  \item Dessiner l'arbre correspondant à cette expérience.
  \item \emph{Évènement «~Y=2~»}
    \begin{enumerate}
      \item Donner une issue correspondant à cet évènement, et calculer sa probabilité.
      \item Faire de même pour un autre exemple. Que constatez-vous ?
      \item Quelle donnée manque-t-il pour calculer $P(Y=2)$ ?
      \item Calculer $P(Y=2)$.
    \end{enumerate}
  \item \emph{Évènement «~Y=3~»}
    \begin{enumerate}
      \item Donner deux issues différentes correspondant à cet évènement, et calculer leurs probabilités. Que constatez-vous ?
      \item Quelle donnée manque-t-il pour calculer $P(Y=3)$ ?
      \item Calculer $P(Y=3)$.
    \end{enumerate}
\end{enumerate}

\subsection{Cent-soixante-neuf répétitions}

On répète maintenant cent-soixante-neuf fois l'expérience (correspondant aux 169 prédictions de l'astrologue). Notons que cette répétition est faite avec remise : on peut tomber plusieurs fois sur la même date. On appelle $Z$ la variable aléatoire représentant le nombre de succès.

L'arbre correspondant à cette expérience contient des milliars de milliards de branches (environ $10^{51}$); nous allons nous en passer.

\begin{enumerate}
  \item Quelles valeurs peut prendre $Z$ ?
  \item Calculer $P(Z=0)$ et $P(Z=169)$.
  \item \emph{Évènement «~Z=33~»}
    \begin{enumerate}
      \item Donner un exemple d'issue correspondant à cet évènement, et calculer sa probabilité.
      \item En utilisant les questions précédente, quelle information nous manque-t-il pour calculer $P(Z=33)$ ?
      \item On appelle \emph{coefficient binomial de 33 et 169}, noté $\binom{169}{33}$, le nombre de branches de l'arbre à 33 succès. On donne : $\binom{169}{33}\approx1,3436\times10^{35}$.
    \end{enumerate}
  \item À titre de comparaison, en utilisant le même genre de raisonnement, calculer la probabilité, sur cent lancers d'une pièce de monnaie équilibrée, d'obtenir exactement cinquante fois pile et cinquante fois face.
  \item À ce stade, peut-on confirmer ou infirmer que le prétendu astrologue a fait preuve de ses pouvoirs ?
\end{enumerate}

\pagebreak

\section{Coefficients binomiaux}

\subsection{Définition}

\begin{definition}[Coefficient binomial]
  Soient $k$ et $n$ deux entiers naturels, tels que $k\leq n$. On considère l'arbre représentant un schéma de Bernoulli de coefficients $n$ et $p$ (pour un certain $p$ réel de $[0, 1]$).

  On appelle \emph{coefficient binomial} de $k$ et $n$, noté $\binom{n}{k}$ (anciennement $C^k_n$), le nombre de chemins de cet arbre réalisant $k$ succès.
\end{definition}

\begin{propriete}[Dénombrement]
  Le nombre de combinaisons de $k$ éléments parmi $n$ est égal à $\binom{n}{k}$.

  Pour cette raison, $\binom{n}{k}$ est parfois prononcé \emph{«~$k$ parmi $n$~»}.
\end{propriete}

\begin{exemple}~
  \begin{itemize}
    \item Le nombre de couples de couleurs différentes parmi un choix de six couleurs est \dotfill%$\binom{6}{2}=15$.
    \item Le tirage du loto est un tirage de sept numéros parmi quarante. Le nombre de tirages possibles est \dotfill %$\binom{40}{7}\approx19\times10^8$.
  \end{itemize}
\end{exemple}

\subsection{Propriétés}

\subsubsection{Travail préliminaire}

Réaliser, sur une feuille de brouillon, \emph{proprement}, les arbres correspondant à deux schéma de Bernoulli de paramètres respectifs $n=3$ et $p$ d'une part, et $n=4$ et $p$ d'autre part.

\subsubsection{Valeurs «~remarquables~»}

\emph{Pour chacune des questions suivantes, essayer d'abord avec des valeurs de $n$ particulières (par exemple $n=2$, $n=3$), puis en déduire une généralisation avec un $n$ quelconque.}

Soit un schéma de Bernoulli de coefficients $n$ et $p$ (pour un entier strictement positif $n$, et un réel $p$ de $\left[ 0;1 \right]$).

\begin{enumerate}
  \item Donner toutes les issues possibles à 0 succès. En déduire $\binom{n}{0}$.
  \item Donner toutes les issues possibles à 1 succès. En déduire $\binom{n}{1}$.
  \item Donner toutes les issues possibles à $n$ succès. En déduire $\binom{n}{n}$.
\end{enumerate}

\subsubsection{Coefficients «~symétriques~»}

\begin{enumerate}
  \item Soit un schéma de Bernoulli de paramètres $n=3$ et $p\in\left[ 0;1 \right]$.
    \begin{enumerate}
      \item Lister tous les évènements possibles à 1 succès, puis tous les évènements possibles à $3-1$ succès. Apparier les éléments des deux listes.
      \item En déduire une relation entre $\binom{3}{1}$ et $\binom{3}{2}$.
    \end{enumerate}
  \item Mêmes questions avec $n=4$, et les évènements à 3 succès d'une part, puis les évènements à $4-3$ succès d'autre part.
  \item En déduire une relation entre $\binom{n}{p}$ et $\binom{n}{n-p}$ (pour un entier $p$ de $\left[ 0;n \right]$).
\end{enumerate}

\subsubsection{Calcul pratique de coefficients binomiaux}

\begin{enumerate}
  \item On s'intéresse à l'arbre correspondant à un schéma de Bernoulli de paramètres $n=3$ et $p$ quelconque.
    \begin{enumerate}
      \item Masquer les branches les plus à droite de l'arbre. La partie visible correspond à un arbre d'un schéma de Bernoulli de paramètres $n=2$, pour le même $p$.
      \item Marquer en rouge les chemins à un succès. En déduire $\binom{2}{1}$.
      \item Marquer en vert les chemins à zéro succès. En déduire $\binom{2}{0}$.
      \item Révéler la partie droite de l'arbre, compter les branches à deux succès, et en déduire $\binom{3}{1}$.
      \item En déduire une relation entre $\binom{3}{1}$, $\binom{2}{1}$ et $\binom{2}{0}$.
    \end{enumerate}
  \item On s'intéresse à l'abre correspondant à un schéma de Bernoulli de paramètres $n=4$, et $p$ quelconque.
    \begin{enumerate}
      \item Masquer les branches les plus à droite de l'arbre.
      \item Marquer en rouge les chemins à trois succès. En déduire $\binom{3}{3}$.
      \item Marquer en rouge les chemins à deux succès. En déduire $\binom{3}{2}$.
      \item Révéler la partie droite de l'arbre, compter les branches à trois succès, et en déduire $\binom{4}{3}$.
      \item En déduire une relation entre $\binom{4}{3}$, $\binom{3}{3}$ et $\binom{3}{2}$.
    \end{enumerate}
  \item \emph{Généralisation :} Soient $n\in\mathbb{N}^*$ et $p$ un entier dans $\left[ 0;n \right]$. Exprimer $\binom{n+1}{p}$ en fonction de $\binom{n}{?}$ et $\binom{n}{?}$ (en remplaçant les $?$ par les bonnes valeurs).
\end{enumerate}

\subsection{Triangle de Pascal}

\vfill

\subsection{Bilan}

\begin{propriete}
  Soient $k$ et $n$ deux entiers naturels, tels que $k\leq n$. Alors

  \begin{enumerate}[(i)]
    \item $\binom{n}{0}=\binom{n}{n}=\ldots$
    \item $\binom{n}{1}=\ldots$
    \item $\binom{n}{n-k}=\ldots$
    \item $\binom{n}{k}+\binom{n}{k+1}=\ldots$
  \end{enumerate}
\end{propriete}

\pagebreak

\section{Échantillonnage}

\subsection{Loi binomiale}

\begin{enumerate}
  \item Dans un tableur, écrire dans les cellules $A1$ à $C1$ les textes : \texttt{k}, \texttt{P(X=k)} et \texttt{P(X<=k)}. Entrer les entiers de 0 à 169 dans les cellules $A2$ à $A171$.
  \item Dans la cellule $B2$, écrire la formule calculant $P(X=0)$ (en faisant référence à $A2$) ; recopier cette formule de $B2$ à $B171$.
  \item Lire et interpréter la valeur affichée en $B35$.
  \item Dans la cellule $C2$, écrire la formule calculant $P(X\leq0)$ (en faisant référence à $A2$) ; recopier cette formule de $C2$ à $C171$.
  \item Lire et interpréter la valeur affichée en $C35$.
\end{enumerate}

\subsection{Échantillonnage}

\begin{enumerate}
  \item Quel sont le plus petit entier $a$ tel que $P(X\leq a)>0,025$, et le plus petit entier $b$ tel que $P(X\leq b)\geq0,975$ ? Justifier que $P(a\leq X\leq b)\geq0,95$.
  \item On considère la variable aléatoire $\frac{X}{n}$, qui comptabilise la \emph{fréquence} de succès du schéma de Bernoulli. Justifier que $P\left( \frac{a}{n}\leq\frac{X}{n}\leq\frac{b}{n} \right)\geq0,95$. À quoi correspond l'intervalle $J=\left[\frac{a}{n};\frac{b}{n}\right]$ ?
  \item Calculer la fréquence de prédictions correctes de l'astrologue. Cette fréquence appartient-elle à $J$ ? Que peut-on conclure ?
\end{enumerate}

\end{document}
