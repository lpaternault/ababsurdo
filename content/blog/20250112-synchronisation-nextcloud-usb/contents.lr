---
title: Nextcloud sur une clef USB
---
description: Voici un tutoriel pour expliquer comment synchroniser son espace Nextcloud sur clef USB
---
date: 2025-01-12
---
image: coucher-de-soleil-en-mediterranee.jpg
---
mastodon_id: 113817897033558875
---
toc: 1
---
tags:

nuage
apps
---
body:

## Résumé

Il est possible de synchroniser son dossier Nextcloud (le logiciel utilisé pour [Nuage](https://nuage.apps.education.fr/)) avec le logiciel [`nextcloudcmd`](https://docs.nextcloud.com/desktop/3.6/nextcloudcmd.html) et la commande suivante :

~~~shell
nextcloudcmd --password MOTDEPASSE "REPERTOIRE" "https://IDENTIFIANT@URL"
~~~

Du coup, stocker dans un fichier `synchroniser.sh` les lignes suivantes permet de l'avoir toujours sous la main.

~~~shell
#!/bin/bash

nextcloudcmd --password MOTDEPASSE "$(dirname $(realpath $0))" "https://IDENTIFIANT@URL"
~~~

Ceci est fais sous GNU/Linux, je n'ai aucune idée de si et comment ça fonctionne avec un autre système d'exploitation.

## Problème

Puisque nos profils sur les ordinateurs du lycée sont quasiment remis à zéro à chaque connexion, il est compliqué de synchroniser un dossier entre un poste du lycée et ma machine personnelle. Donc je ne le fais pas, et j'utilise ma clef USB pour cela.

J'utilise deux outils pour cette synchronisation : [git](https://git-scm.com/) (avec un dépôt sur la [Forge des communs numériques éducatifs](https://forge.apps.education.fr/) pour les dépôts publics, ou en local sur mon ordinateur pour les dépôts privés) et [Nuage](https://nuage.apps.education.fr), le service [Nextcloud](https://nextcloud.com/) proposé par notre employeur.

Si `git` fonctionne sans aucun problème sur clef USB, je n'ai pas réussi à configurer, avec le client lourd, la synchronisation d'un répertoire Nextcloud sur clef USB : ce client s'attend à ce que le disque soit en permanence disponible, ce qui n'est pas le cas d'une clef USB.

## Solution : ligne de commande

Nextcloud fournit un outil en ligne de commande, qui permet de faire *une seule synchronisation* à l'exécution, plutôt que de surveiller les modifications du répertoire, ce qui est exactement ce que je veux.

### Installer l'application

Les détails sont donnés [dans la documentation de Nextcloud](https://docs.nextcloud.com/desktop/3.6/nextcloudcmd.html#install-nextcloudcmd).

### Déterminer son identifiant, et définir un mot de passe d'application

- Votre « ID de cloud fédéré » est affiché sur Nextcloud, dans les paramètres, dans la section « Partage » :

  ![Comment voir l'ID de cloud fédéré](id-cloud-federe.png)

- Créer un mot de passe d'application se fait, toujours dans les paramètres, dans la section « Sécurité » :

  ![Générer un mot de passe d'application](mot-de-passe1.png)

  Une fois validé, vous obtenez la fenêtre suivante, de laquelle vous pouvez copier le mot de passe (attention, il n'est sauvegardé nulle part) :

  ![Copier le mot de passe d'application](mot-de-passe2.png)

### Commande pour synchroniser

Pour synchroniser votre espace Nextcloud sur la clef USB, en ligne de commande, placez-vous dans le répertoire désiré (sur la clef USB), puis lancez la commande suivante (en remplaçant `IDENTIFIANT@URL` par l'ID de cloud fédéré obtenue précédemment, et `MOTDEPASSE` par le mot de passe d'application généré à la partie précédente.

~~~shell
nextcloudcmd --password MOTDEPASSE . "https://IDENTIFIANT@URL"
~~~

### Automatisation

Pour ne pas perdre ces informations, j'ai à la racine de mon espace Nuage le fichier `synchroniser.sh` contenant les quelques lignes :

~~~bash
#!/bin/bash

nextcloudcmd --password MOTDEPASSE "$(dirname $(realpath $0))" "https://IDENTIFIANT@URL"
~~~

Le `"$(dirname $(realpath $0))"` est un peu de magie permettant d'obtenir le répertoire dans lequel se situe le fichier : ainsi, je peux l'exécuter de n'importe où, et c'est toujours le répertoire contenant ce fichier qui est synchroniser.

Pour effectuer la synchronisation, il ne me reste plus qu'à exécuter ce fichier en console :

~~~shell
$ ./synchroniser.sh
~~~

## Bilan

La mise en place est fastidieuse, mais une fois que ça marche, ça fonctionne tout seul.
