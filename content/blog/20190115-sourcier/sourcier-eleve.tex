%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
\documentclass[11pt]{article}


\usepackage{textcomp}
\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{graph35}
\usepackage[a4paper, margin=1cm]{geometry}
\usepackage{amsthm}
\usepackage{hyperref}
\hypersetup{
  unicode=true,
  urlcolor=cyan,
  pdfauthor={Louis Paternault},
  pdfproducer={© Louis Paternault — CC-BY-SA-4.0 — http://ababsurdo.fr},
  hidelinks,
}

\newtheorem*{activite}{Activité}
\newtheorem*{defprop}{Définition et Propriété}
\newtheorem*{methode}{Méthode}
\newtheorem*{exemple}{Exemple}

\newcommand{\reponse}{\emph{Réponse : .......................}}

% Compléter les trous
\usepackage{censor}
\censorruledepth=-.2ex
\censorruleheight=.1ex
\newcommand{\blanc}[1]{\censor{#1}}

% Unités
\RequirePackage{siunitx}
\sisetup{locale = FR,
  detect-all,
  group-minimum-digits=4,
}

\setlength{\parindent}{0pt}

\begin{document}

\setcounter{section}{3}
\section{Échantillonnage}

\begin{activite}
  Un «~sourcier~» accepte de se préter à une expérience pour prouver son pouvoir : il essaye de déterminer si de l'eau traverse un tuyau.

  Sur 50 essais, il a trouvé la réponse correcte 30 fois.
  Le sourcier a-t-il apporté la preuve de son pouvoir ?

  Pour répondre à cette question, nous allons voir si une personne soumise au même test que le sourcier, mais répondant au hasard, pourrait obtenir un aussi bon taux de réussite que le prétendu sourcier.

  \begin{enumerate}
    \item En annonçant au hasard si le seau contient ou non de l'eau, quelle est la probabilité d'obtenir la bonne réponse ? \reponse
    \item Simuler une expérience : sur la calculatrice, tirer un nombre entre 0 et 1 au hasard :
      \key{OPTN}
      \function{PROB-b}
      \function{RAND-b}
      \function{Ran-b}.
      Si ce nombre est supérieur à 0,5, compter une bonne réponse (succès) ; sinon, compter une mauvaise réponse (échec). \reponse
    \item Le sourcier a eu droit à 50 essais. Simuler 50 expériences (en répétant les mêmes instructions qu'à la question précédente), et noter les résultats ci-dessous. Dans chaque case, mettre un cercle $\circ$ en cas de succès, et une croix $\times$ en cas d'échec.
      \begin{center}
        \begin{tabular}{*{25}{|c}|}
          \hline
          &&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
          &&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
        \end{tabular}
      \end{center}
      \emph{Nombre total de succès obtenus : \ldots}
    \item Partager les résultats avec l'ensemble de la classe (pour plus de lisibilité, laisser vide les cases contenant 0).
      \begin{center}
        \setlength\tabcolsep{1.5pt} % default value: 6pt
        \begin{tabular}{|l|*{26}{|c}|}
          \hline
          Succès & 0&1&2&3&4&5&6&7&8&9&10&11&12&13&14&15&16&17&18&19&20&21&22&23&24&25\\
          \hline
          Effectif & &&&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
          \hline
          Succès & 26&27&28&29&30&31&32&33&34&35&36&37&38&39&40&41&42&43&44&45&46&47&48&49&50&\\
          \hline
          Effectif & &&&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
        \end{tabular}
      \end{center}
    \item Combien de simulations ont produit autant ou plus de succès que le sourcier ? \reponse
    \item Le sourcier a-t-il apporté la preuve de son pouvoir ?
  \end{enumerate}
\end{activite}

\subsection*{Intervalle de fluctuation}

\begin{defprop}
  Pour un échantillon de taille $n\geq25$, et une proportion $p$ du caractère appartenant à $[0,2;0,8]$, la fréquence observée d'apparition d'un caractère dans l'échantillon appartient à l'\blanc{intervalle de fluctuation} \blanc{$\left[p-\frac{1}{\sqrt{n}};p+\frac{1}{\sqrt{n}}\right]$} avec une probabilité d'au moins \blanc{$0,95$}.
\end{defprop}

\begin{methode}[Prise de décision]
  Étant donné un échantillon de taille $n\geq25$, on se demande s'il est compatible avec un modèle donné, où le caractère apparait avec une probabilité $p\in\left[ 0,2;0,8 \right]$. On note $f$ la fréquence d'apparition du caractère dans cet échantillon.
  \begin{itemize}
    \item Si $f\notin\left[ p-\frac{1}{\sqrt{n}};p+\frac{1}{\sqrt{n}} \right]$, \phantom{on peut rejeter l'hypothèse que l'échantillon soit compatible avec le modèle.}
      \vspace{\stretch{1}}
    \item Si $f\in\left[ p-\frac{1}{\sqrt{n}};p+\frac{1}{\sqrt{n}} \right]$, \phantom{on ne peut pas rejeter l'hypothèse que l'échantillon soit compatible avec le modèle.}
        \vspace{\stretch{1}}
  \end{itemize}
\end{methode}

\begin{exemple}
  L'Assemblée nationale est actuellement composée 576 députés, dont 228 femmes\footnote{Source : \emph{Liste des députés répartis par sexe}, Assemblée Nationale, \url{http://www2.assemblee-nationale.fr/deputes/liste/homme-femme} (consulté le 14/11/2018).}, alors que
  la population française est composée de 48\% de femmes\footnote{Source : \emph{Population par sexe et groupe d'âges en 2018}, \textsc{Insee}, \url{https://www.insee.fr/fr/statistiques/2381474} (consulté le 14/11/2018).}.
  La question que l'on se pose est : La proportion de femmes à l'Assemblée est-elle normale, ou sont-elles sous-représentées ?

  \begin{enumerate}
    \item Quelle est la proportion de femmes à l'Assemblée nationale ?
    \item On choisit au hasard un échantillon de 576 personnes dans la population française.
      \begin{enumerate}
        \item Quelle est la taille de l'échantillon ? Quelle est la probabilité qu'un individu choisi au hasard parmi les français de la population française soit une femme ?
        \item L'échantillon respecte-t-il les conditions de l'intervalle de fluctuation ?
        \item Déterminer un intervalle de fluctuation au seil de \SI{95}{\%} de cet intervalle.
      \end{enumerate}
    \item Conclure : Les femmes sont-elles sous-représentées à l'Assemblée nationale ?
  \end{enumerate}
\end{exemple}

\end{document}
