title: Univers infini
---
description: Étude d'une expérience aléatoire à l'univers infini, et prise de recul sur les limites de la méthode utilisée
---
date: 2015-10-20
---
image: galaxy.jpg
---
toc: 0
---
tags:

première S
loi binomiale
probabilités
suites
DM
---
body:

*Remarque : Une [version plus ancienne](/blog/20141024-univers-infini) de cet article existe.

Voici un exercice que j'ai proposé en devoir à la maison, qui a pour moi deux
intérêts.

- La première partie est plutôt classique, et propose un exercice
  d'approfondissement sur la notion d'espérance.
- La dernière question est pour moi plus intéressante. Elle demande un peu de
  réflexion, en analysant les limites de la méthode étudiée dans les questions
  précédentes.

Il pourrait être intéressant d'ajouter un peu d'algorithmique en commençant par
conjecturer, à l'aide d'un algorithme, la valeur de l'espérance $E(X)$.

* [Énoncé](univers_infini.pdf)
* [Source](univers_infini.tex)

## Mise à jour du 20 octobre 2015

Ce à quoi je m'attendais en posant la question $3.$ (calcul de l'espérance) était de résoudre l'équation donnée à la question précédente $E(X)=\frac{21+E(X)}{6}$, l'inconnue étant $E(X)$. Une année où j'ai proposé ce devoir, j'ai plusieurs fois vu l'erreur suivante. Plutôt que de résoudre cette équation (soit que l'énoncé n'ait pas été compris, soit qu'une inconnue $E(X)$ soit trop déroutante), des élèves ont calculé une première fois l'espérance en ignorant l'inconnue de droite $E(X)=\frac{21}{6}=\frac{7}{2}$ (plutôt que de résoudre $E(X)=\frac{21+E(X)}{6}$), puis ont recalculé l'espérance avec cette nouvelle valeur : $E(X)=\frac{21+E(X)}{6}=\frac{21+\frac{7}{2}}{6}=\frac{49}{12}$.

Cette méthode, et le résultat qui en découle, sont faux.

Je l'ai toutefois présentée à la classe, en leur demandant (rapidement) de déceler et analyser l'erreur. Puis je leur ai proposé de persister dans l'erreur : une fois $E(X)=\frac{49}{12}$ calculé, appliquons à nouveau la même méthode : $E(X)=\frac{21+E(X)}{6}=\frac{21+\frac{49}{12}}{6}\approx 4,18$. Puis recommençons : $E(X)=\frac{21+E(X)}{6}\approx\frac{21+4,18}{6}\approx4,20$. Puis recommençons, encore et encore, à la calculatrice. Nous observons qu'assez vite, le résultat est très proche de la valeur de $E(X)$ recherchée. En effet (mais je ne leur ai pas expliqué cela), la suite arithmético-géométrique de premier terme quelconque et d'expression $u_{n+1}=\frac{21+u_n}{6}$ converge vers son unique point fixe $\frac{21}{5}$.

Nous avons donc observé que si la méthode repose sur une erreur de raisonnement, et ne permet pas de calculer de manière exacte la solution, elle permet néanmoins de s'approcher assez vite de la bonne réponse. Cela n'est pas anodin : les scientifiques sont fréquemment confrontés à des équations trop complexes pour que la solution soit calculée de manière exacte, mais dont une solution approchée peut être calculée, notamment (mais pas exclusivement) par une méthode itérative.

{% from 'jinjamacros/images.html' import image with context %}
{{ image("ybc7289.jpg") }}

Un autre exemple de ce calcul approché en utilisant une suite convergente est le calcul d'une valeur approchée de $\sqrt{2}$ par la [méthode de Héron](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_H%C3%A9ron), retrouvée (peut-être) sur la [tablette d'argile YBC 7289](https://fr.wikipedia.org/wiki/YBC_7289) (illustration ci-dessus).

### Bilan

Cette erreur a été très riche. Elle a permis, en vrac :

* de montrer une manière de trouver la solution approchée d'une équation particulière ;
* d'expliquer que parfois, les scientifiques se contentent de solution approchée (et construisent des ponts et des avions avec de telles solutions) ;
* de faire un peu d'histoire des mathématiques ;
* d'introduire le chapitre sur les suites, qui, heureux hasard du calendrier, commençait à ce moment-là.

