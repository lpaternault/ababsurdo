---
title: Clef USB de travail
---
description: Description du contenu de ma clef USB, me permettant d'avoir avec moi mes cours et les logiciels pour les travailler ($\LaTeX$, Python, vim, une console, etc.).
---
date: 2015-06-15
---
image: dead_drop.jpg
---
toc: 1
---
tags:

technique
---
body:


*Avertissement : Cet article peut être considéré comme plutôt technique. Le but
n'est pas tellement d'expliquer pourquoi et comment j'utilise des outils
informatiques, mais plutôt d'expliquer à celles et ceux qui les utilisent déjà
comment les utiliser partout avec une clef USB.*

Cela m'a pris pas mal de temps de mettre en place cette clef USB, donc je
partage les informations utiles.

## Préambule et Besoins

Je travaille exclusivement avec GNU/Linux ([Debian](http://debian.org) pour
être plus précis). J'utilise énormément la ligne de commande. À de rares
exceptions près, tous les documents que je crée sont écrits en
[$\LaTeX$](http://latex-project.org/), et versionnés avec
[git](http://git-scm.org). Je bidouille de temps en temps en
[Python](https://www.python.org/) (plutôt Python3) pour mes cours.

Mes besoins sont :

- pouvoir synchroniser automatiquement mes cours sur ma clef USB, et ceux sur
  mon ordinateur, pour ne rien perdre ;
- pouvoir, au lycée, corriger lesdits cours (en $\LaTeX$), et pouvoir compiler
  ce document ;
- pouvoir utiliser git pour enregistrer les modifications.

Tout ceci se fait sur une clef USB de 4Go aux couleurs du lycée.

## Cours

Mes cours sont versionnés avec git, et ma clef USB contient des clones des
dépôts de tous mes cours (je fais un dépôt par classe). La synchronisation est
donc évidente. Seuls les fichiers $\LaTeX$ sont versionnés, pas les PDF. Pour
pouvoir synchroniser tous ces dépôts en une commande, j'utilise
[myrepos](https://myrepos.branchable.com/), avec un fichier `.mrconfig` placé
sur la clef USB.

Pour reconstruire automatiquement les PDF des fichiers modifiés, j'utilise
[Évariste](https://framagit.org/spalax/evariste/) (à l'heure où j'écris
ces lignes, ce logiciel est encore en développement, et très peu documenté) ;
ce logiciel n'a pas été écrit dans ce but, mais il se trouve qu'il remplit très
bien cette tâche.

## Logiciels

### [Framakey](http://framakey.org)

L'association [Framasoft](http://framasoft.org) réalise un très bon travail de
rédaction et compilation d'applications portables, ainsi qu'un gestionnaire de
paquet pour faciliter l'installation et la mise à jours desdits logiciels. Tout
ceci est disponible dans un logiciel à installer sur clef USB.

Entre autres logiciels, on trouvera Firefox, gVim, un visionneur de PDF, etc.

### [Cygwin](https://www.cygwin.com/)

À l'époque où la [Framakey LaTeX](https://framakey.org/Pack/Framakey-LaTeX)
n'était pas encore disponible (je n'ai pas testé cette dernière ; je ne sais
pas ce qu'elle vaut, même si j'ai plutôt confiance en le travail de Framasoft),
j'ai installé Cygwin pour pouvoir retrouver mes outils favoris en ligne de
commande, dont LaTeX.

#### Téléchargement et Installation

[Une version portable](https://github.com/CybeSystems/CygwinPortable) de
Cygwin a été réalisée
([téléchargement](http://www.cybesystems.com/index.php/downloads)). Après
installation sur la clef USB, il est possible d'installer de nouveaux
logiciels.

#### Raccourci

Un problème avec cette méthode est qu'on ne sait pas à l'avance, avant de
brancher la clef, quelle lettre de lecteur va lui être attribué. Cela rend
difficile l'écriture de scripts, et l'ajout de chemins au ``PATH`` (décrit dans
la partie *MikTeX*). Voici comment corriger cela.

Jeff Valore [a écrit](http://codingwithspike.wordpress.com/2012/08/02/making-a-truly-portable-cygwin-install-working-around-drive-letter-and-file-permission-issues)
un article détaillant comment rendre Cygwin portable. Je n'ai pas tout appliqué
(Cygwin portable en rend une bonne partie inutile), mais j'ai tout de même
extrait des choses intéressantes, notamment le lanceur suivant (fichier
``.bat``).

```bat
REM Get current drive letter into WD.
for /F %%A in ('cd') do set WD=%%~dA

REM Start cygwin
portable\cygwin\CygwinPortable.exe

REM Open a terminal
portable\cygwin\CygwinPortable.exe -path .
```

Lorsque j'exécute ce script, il lance cygwin et ouvre une console, et je peux
ensuite récupérer la lettre de lecteur dans la variable d'environnement ``WD``.
La commande suivante, dans le ``.bashrc``, permet d'affecter le chemin absolu
de la racine de la clef USB à la variable ``keydir``.

```bash
## Définition d'une variable vers le chemin absolu de la clef, et d'autres variables
keydir="/cygdrive/$(echo ${WD,,} | cut -c1)"
```

#### Mise à jour

La version portable fournie est assez vieille, et beaucoup de paquets manquent.
Pour pallier ce problème, j'ai installé la version
[setup-x86.exe](https://cygwin.com/setup-x86.exe) fournie par Cygwin (donc pas
prévue, à priori, pour être portable). Jusqu'ici, tout semble fonctionner…

### $\LaTeX$

L'équipe de MikTeX [fournit une version portable](http://miktex.org/portable),
donc le gros du travail est fait. Il reste ensuite à l'intégrer à la clef.

- La commande suivante dans le ``.bashrc`` permet d'ajouter le répertoire de
  MikTeX au ``PATH``, rendant les différentes commandes $\LaTeX$ accessibles
  depuis la console cygwin.

  ```bash
  # Ajout de LaTeX (par miktex) au path
  export PATH="$PATH:$keydir/portable/miktex/miktex/bin"
  ```

- Enfin, la commande suivante permet d'ajouter [mes classes personnalisées](https://forge.apps.education.fr/paternaultlouis/pablo) à
  l'environnement $\LaTeX$. Ces classes sont dans leur propre dépôt git sur la
  clef USB ; reste à ajouter le répertoire à l'environnement $\LaTeX$, toujours
  dans le ``.bashrc``.

  ```bash
  # Mes classes LaTeX
  export TEXINPUTS="$TEXINPUTS;$(echo $keydir | sed 's+.*/\([^/]*\)+\U\1+'):/prof/latex"
  ```

  Notons que la syntaxe diffère de l'habitude unixienne (par l'utilisation d'un
  point virgule pour séparer les chemins), et qu'une petite manipulation est
  nécessaire pour récupérer la lettre du lecteur, en majuscule.

### Python

J'ai essayé différentes manières d'installer Python, mais le fait que
l'installation doive être à la fois portable et sur une clef USB m'a donné du
fil à retordre. J'ai essayé :

- [PythonPortable](http://portablepython.com), qui était à mon avis la
  meilleure solution, mais n'est plus maintenu ;
- [WinPython](http://winpython.github.io/), mais il inclut par défaut beaucoup
  trop de paquets pour mon utilisation (même si la plupart peuvent être
  désinstallés par la suite), et il ne semble pas être conçu pour une
  utilisation conjointe des deux versions python2 et python3 : lors de mes
  tests, les scripts et binaires se mélangeaient les pinceaux entre les deux
  versions de WinPython installées, et la version fournie par Cygwin.

J'ai finalement opté pour l'installation des versions de Python 2 et 3 fournies
par Cygwin, mais `pip` a posé problème.

#### Pip

Malheureusement, [pip](https://pypi.python.org/pypi/pip) (gestionnaire de
paquets pour Python, permettant l'installation de nouveaux paquets et
programmes) n'est fourni par Cygwin ni pour python2 ni pour python3. Il a donc
fallu l'installer en suivant [les instructions officielles](https://pip.pypa.io/en/latest/installing.html#install-pip).

Le problème de `pip` sur clef USB (à moins que ce ne soit `pip` avec Cygwin) est
que l'obtention d'un verrou sur un fichier (avec le module
[lockfile](https://github.com/pypa/pip/blob/develop/pip/utils/outdated.py#L71))
ne fonctionne pas, ou alors pas toujours, et l'installation bloquait à cet
endroit. Pour remédier à cela, j'ai, en utilisant
[pdb](https://docs.python.org/2/library/pdb.html), mis le programme en pause
une fois `pip` téléchargé dans un répertoire temporaire, et modifié la fonction
[load_selfcheck_statefile](https://github.com/pypa/pip/blob/develop/pip/utils/outdated.py#L88-92)
pour utiliser `VirtualenvSelfCheckState` plutôt que `GlobalSelfCheckState`. Je
ne suis pas sûr des conséquences, mais cela a fonctionné.

Enfin, j'ai pu installer des paquets et programmes avec `pip`. J'ai été
confronté au même problème avec `lockfile`, donc j'ai à nouveau fait la même
manipulation que précédemment, mais dans le répertoire d'installation de `pip`
cette fois-ci, et j'ai enfin été capable d'installer
[pdfautonup](https://framagit.org/spalax/pdfautonup/), et de l'utiliser.

#### Bilan

Pas évident, beaucoup de bidouilles, mais ça fonctionne…

### Mathématiques

Quelques programmes mathématiques.

- [Géogebra](http://www.geogebra.org/download/index.php?os=win&portable=true)
  ([détails](http://wiki.geogebra.org/en/Reference:GeoGebra_Installation))fournit
  une version portable du logiciel.
- [xcas](http://www-fourier.ujf-grenoble.fr/~parisse/giac_fr.html) est portable par essence ; il n'y a donc rien de spécial à faire.

## Sauvegardes

Cela me ferait de la peine de perdre tout ce travail, c'est pourquoi [je fais régulièrement des sauvegardes](https://rsync.samba.org/).

