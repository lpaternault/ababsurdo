---
title: Encore les tâches domestiques
---
description: Amélioration de ma séance sur les tâches domestiques
---
date: 2023-08-18
---
image: pinning-our-hopes-on-spring.jpg
---
toc: 1
---
tags:

sexisme
SES
sociologie
statistiques
transdisciplinaire
---
body:

Même si j'avais beaucoup aimé le [travail effectué](../20190123-taches-domestiques) sur la répartition genrée des tâches domestiques, je ne l'ai pas refait pendant plusieurs années, notamment (mais pas uniquement) parce que le collègue de SES était remplaçant qui est finalement resté toute l'année, mais qui ne savait jamais s'il serait encore là un mois plus tard…

## Déroulement

J'ai enfin pu refaire cette séance avec une collègue de SES. Nous avons suivi le même déroulé que [la précédente](../20190123-taches-domestiques), à savoir :

- en cours de mathématiques :
  - sondage sur le temps hebdomadaire passé à réaliser des tâches domestiques, puis analyse des résultats avec les connaissances du cours (en utilisant des boîtes à moustache, ce qui n'est pas au programme, mais pertinent dans ce cas-là) ;
  - statistiques sur la représentation des enfants dans des catalogues de jouets de Noël (en utilisant des tableaux croisés d'effectifs) ;
- en cours de SES :
  - exploitation du travail de mathématiques pour répondre, sous la forme d'un paragraphe argumenté, à la question « La socialisation a-t-elle une influence sur la répartition des tâches domestiques ? », ce qui s'intègre très bien au programme.

## Téléchargement

- [calculs.pdf](calculs.pdf) ([source](calculs.tex))
- [resultats.pdf](resultats.pdf) ([source](resultats.tex))
- [sondage.pdf](sondage.pdf) ([source](sondage.tex))

## Mathématiques

### Sondage

J'ai amélioré [le document](sondage.pdf) ([source](soundage.tex)) distribué pour le sondage, ce qui m'a permis d'obtenir beaucoup plus de réponses que la fois précédente (76 réponses pour une classe de plus de 30 élèves).

### Calculs

J'ai récupéré les résultats *la veille* de la séance sur les calculs. Cela m'a permis de faire le travail de dépouillement tranquillement plutôt qu'en direct devant ou avec les élèves.

J'ai commis l'erreur de permettre à quelques retardataires d'apporter leurs résultats le jour même, ce qui m'a empêché de distribuer aux élèves [un document](calculs.pdf) ([source](calculs.tex)) avec toutes les valeurs. Au lieu de cela, j'ai demandé aux élèves de recopier [le tableau](taches-domestiques.csv) sur leur cahier, ce qui a pris au moins dix minutes (si ne n'est plus) pour certains élèves (alors qu'il m'en faut une poignée pour faire le même travail).

### Résultats

J'ai à chaque fois un doute : Et si la population étudiée n'est pas représentative, et les résultats montrent que les hommes passent plus de temps à réaliser des tâches domestiques que les femmes ?

Encore une fois, je n'ai (malheureusement ?) [pas été déçu](resultats.pdf) ([source](resultats.tex)).

## Conclusion

J'ai l'impression que de moins en moins de magasins distribuent des catalogues de Noël. C'est une bonne chose, mais ça complique mon travail…

C'est une affirmation très subjective, mais j'ai l'impression que les élèves étaient plus investis pour le calcul des statistiques pour cet exemple concret s'appuyant sur des données qu'ils et elles ont récoltées, plutôt que pour des exercices plus classiques du manuel. Tant mieux, c'est le but recherché !
