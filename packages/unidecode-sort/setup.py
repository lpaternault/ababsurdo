import ast
import io
import re

from setuptools import setup, find_packages

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

_description_re = re.compile(r"description\s+=\s+(?P<description>.*)")

with open("lektor_unidecode_sort.py", "rb") as f:
    description = str(
        ast.literal_eval(_description_re.search(f.read().decode("utf-8")).group(1))
    )

setup(
    author="Louis Paternault",
    author_email="spalax@gresille.org",
    description=description,
    keywords="Lektor plugin",
    license="BSD-3-Clause",
    long_description=readme,
    long_description_content_type="text/markdown",
    name="lektor-unidecode-sort",
    packages=find_packages(),
    py_modules=["lektor_unidecode_sort"],
    url="https://forge.apps.education.fr/paternaultlouis/ababsurdo/-/tree/main/packages/unidecode-sort",
    version="1.0",
    classifiers=[
        "Framework :: Lektor",
        "Environment :: Plugins",
        "License :: OSI Approved :: BSD License",
    ],
    install_requires=["unidecode"],
    entry_points={
        "lektor.plugins": [
            "unidecode-sort = lektor_unidecode_sort:UnidecodeSortPlugin",
        ]
    },
)
