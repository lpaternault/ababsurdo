# Unidecode Sort

Add filters `unidecode_sort()` and `unidecode_dictsort()`, which act exactly as the jinja filters `sort()` and `dictsort()`, but processing values using [unidecode.unidecode()](https://pypi.org/project/Unidecode/).

This is a quick-and-dirty way to make `sort()` and `dictsort()` handle diacritics (e.g. Ö, é, Ç…) correctly.
