# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project.  If not, see <http://www.gnu.org/licenses/>.

"""Publication d'un article de blog (changement de la date de 01-01-2222 à aujourd'hui."""

import datetime
import glob
import os
import logging
import subprocess
import sys

from .. import BLOGDIR, DEFAULTDATE

PATTERN = os.path.join(
    BLOGDIR, "{}-*".format(DEFAULTDATE.strftime("%Y%m%d")), "contents.lr"
)


def select():
    """Propose à l'utilisateur de choisir l'article à publier."""
    choices = {
        number: os.path.basename(os.path.dirname(directory))
        for number, directory in enumerate(glob.glob(PATTERN))
    }
    if not choices:
        return None
    while True:
        print("-" * (4 + max(len(article) for article in choices.values())))
        for number, article in choices.items():
            print(f"[{number}] {article}")
        choice = input("Numéro de l'article à publier ? [0] ")
        if choice == "":
            choice = 0
        try:
            return choices[int(choice)]
        except (ValueError, KeyError):
            print("Sélection invalide.")


def _newname(article):
    return article.replace(
        DEFAULTDATE.strftime("%Y%m%d"), datetime.date.today().strftime("%Y%m%d")
    )


def publish(slug):
    """Change la date de l'article donné en argument.

    L'argument est le nom du répertoire représentant l'article.
    """
    # Renommage de l'article
    subprocess.run(["git", "mv", slug, _newname(slug)], cwd=BLOGDIR)

    # Changement de la date
    subprocess.run(
        [
            "sed",
            "--in-place=.bak",
            "s/{}/{}/g".format(
                DEFAULTDATE.isoformat(), datetime.date.today().isoformat()
            ),
            os.path.join(_newname(slug), "contents.lr"),
        ],
        cwd=BLOGDIR,
    )

    # Recherche des articles liants
    referees = subprocess.run(
        ["git", "grep", "-l", slug],
        stdout=subprocess.PIPE,
        text=True,
        cwd=BLOGDIR,
    ).stdout.split()

    # Changement des liens vers le nouvel article
    if referees:
        subprocess.run(
            [
                "sed",
                "--in-place=.bak",
                "s/{}/{}/g".format(
                    DEFAULTDATE.strftime("%Y%m%d"), datetime.date.today().strftime("%Y%m%d")
                ),
            ]
            + referees,
            check=True,
            cwd=BLOGDIR,
        )


if __name__ == "__main__":
    article = select()
    if article is None:
        logging.error("Aucun article en attente de publication.")
        sys.exit(0)
    publish(article)
