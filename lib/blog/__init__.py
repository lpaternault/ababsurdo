# Copyright 2018 Louis Paternault
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project.  If not, see <http://www.gnu.org/licenses/>.

"""Gestion des articles de blog."""

import datetime
import logging
import os
import sys

if sys.version_info < (3, 7):
    logging.error("Ce programme nécessite Python version 3.7 ou supérieure.")
    sys.exit(1)


BLOGDIR = os.path.abspath(
    os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, "content", "blog")
)

DEFAULTDATE = datetime.date(2222, 1, 1)
