# Copyright 2018 Louis Paternault
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this project.  If not, see <http://www.gnu.org/licenses/>.

"""Création d'un nouvel article de blog."""

from urllib.parse import urlparse
import datetime
import logging
import os
import subprocess

from bs4 import BeautifulSoup
from slugify import slugify
import jinja2
import requests

from .. import BLOGDIR, DEFAULTDATE


def download_page(url):
    """Télécharge la page web, et retourne son code HTML."""
    request = requests.get(url)
    request.raise_for_status()
    return request.text


def download_file(url, dest):
    """Télécharge l'URL, et retourne son contenu."""
    request = requests.get(url, stream=True)
    request.raise_for_status()
    request.raw.decode_content = True
    with open(dest, mode="wb") as destfile:
        destfile.write(request.raw.data)


def ask(prompt, default=None, convert=lambda x: x):
    """Pose une question à l'utilisateur, et renvoit la réponse.

    :param str prompt: Question à poser.
    :param default: Réponse par défaut, si l'utilisateur renvoit une réponse vide.
    :param convert: Fonction appliquée à la réponse avant de la renvoyer.
    """
    if default is None:
        defaulttext = ""
    else:
        defaulttext = f"[{default}] "
    while True:
        answer = input(f"{prompt} {defaulttext}")

        try:
            if answer:
                return convert(answer)
            if default is not None:
                return convert(default)
        except Exception as exception:  # pylint: disable=broad-except
            print("Error:", str(exception))


def _parse_licenseurl(url):
    """Analyse la licence et la renvoit sous forme de texte et d'URL.

    Renvoit un tuple `(texte, url)`,
    où `texte` est la licence sous forme lisible par un humain, et `url` son URL.
    """
    urlresult = urlparse(url)
    if urlresult.netloc == "creativecommons.org":
        variante, version = urlresult.path.strip("/ ").split("/")[1:3]
        return (
            f"Creative Commons {variante} {version}",
            f"https://creativecommons.org/licenses/{variante}/{version}/deed.fr",
        )

    return ask("Licence ?"), ask("URL de la licence ?")


class Image:
    """L'image d'un article de blog."""

    # pylint: disable=no-member

    attr_list = [
        "url",
        "title",
        "slug",
        "author",
        "authorurl",
        "license",
        "licenseurl",
        "imageurl",
        "extension",
    ]

    def __init__(self, url):
        # pylint: disable=unused-argument
        for attr in self.attr_list:
            setattr(self, attr, ask(f"Image : {attr} ?", getattr(self, attr, "TODO")))

    @classmethod
    def from_url(cls, url):
        """Renvoit un objet `Image` depuis une URL."""
        urlresult = urlparse(url)
        if urlresult.netloc == "www.flickr.com":
            return FlickrImage(url)
        return cls(url)

    @property
    def context(self):
        """Renvoit les attributs sous forme de dictionnaire."""
        return {attr: getattr(self, attr) for attr in self.attr_list}

    def generate(self, jinjaenv, dirname):
        """Télécharge l'image, et écrit les métadonnées."""
        imagepath = os.path.join(dirname, f"{self.slug}.{self.extension}")
        download_file(self.imageurl, imagepath)
        with open(f"{imagepath}.lr", mode="w") as metafile:
            metafile.write(jinjaenv.get_template("image.lr").render(**self.context))


class FlickrImage(Image):
    """L'illustration d'un article, tirée de Flickr.com"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, url):
        document = BeautifulSoup(download_page(url), "lxml")
        self.url = url
        self.imageurl = ask("URL de l'image ?")
        self.title = document.find_all("h1", class_="photo-title")[0].text.strip()
        self.slug = slugify(self.title)
        self.author = document.find_all("a", class_="owner-name")[0].text.strip()
        self.authorurl = (
            "http://flickr.com" + document.find_all("a", class_="owner-name")[0]["href"]
        )
        self.license, self.licenseurl = _parse_licenseurl(
            document.find_all("a", class_="photo-license-url")[0]["href"]
        )
        self.extension = self.imageurl.split(".")[-1]
        super().__init__(url)


class Article:
    """Un nouvel article de blog."""

    def __init__(self):
        print("Création d'un nouvel article.")
        self.title = ask("Titre ?")
        self.slug = ask("Slug ?", slugify(self.title))
        self.summary = ask("Résumé ?", "TODO")
        self.mastodon = ask("Id du post Mastodon correspondant ?", "TODO")
        self.date = ask(
            "Date (YYYY-MM-DD) ?", DEFAULTDATE.isoformat(), datetime.date.fromisoformat
        )
        self.tags = ask(
            "Étiquettes (séparées par des espaces) ?", "", lambda l: l.split()
        )
        self.image = Image.from_url(ask("URL de la page de l'image ?"))

    @property
    def context(self):
        """Renvoit les attributs sous forme de dictionnaire."""
        return {
            "date": self.date.isoformat(),
            "image": self.image,
            "summary": self.summary,
            "mastodon": self.mastodon,
            "tags": self.tags,
            "title": self.title,
        }

    @property
    def dirname(self):
        return os.path.join(
            BLOGDIR,
            "{date}-{slug}".format(date=self.date.strftime("%Y%m%d"), slug=self.slug),
        )

    def generate(self):
        """Écrit le fichier `contents.lr` de l'article."""
        os.mkdir(self.dirname)
        contentsname = os.path.join(self.dirname, "contents.lr")
        jinjaenv = jinja2.Environment(loader=jinja2.PackageLoader("blog.new"))

        # Génération de l'article
        with open(contentsname, "w") as contents:
            contents.write(jinjaenv.get_template("contents.lr").render(self.context))

        # Génération de l'image
        self.image.generate(jinjaenv, self.dirname)

    def edit(self):
        """Ouvre un éditeur de textes pour modifier le fichier."""
        if "EDITOR" not in os.environ:
            logging.error("Aucun éditeur spécifié… Définisser la variable d'environnement EDITOR pour corriger cela.")
            return

        subprocess.run(
                [os.environ['EDITOR'], "contents.lr"],
                cwd=self.dirname,
                check=True,
                )


if __name__ == "__main__":
    article = Article()
    article.generate()
    article.edit()

