Mon site web : http://ababsurdo.fr

Les documents dont je suis l'auteur (la plupart du contenu) sont publiés sous
licence [Creative Commons by-sa
4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), les autres
documents sont publiés sous des licences autorisant la libre diffusion.

Pour compiler le site web :

- Installer [lektor](http://getlektor.com)

        pip install lektor

- Lancer la compilation

        make serve
